# Payment Methods 

> BPI

- Bank of the Philippine Islands
- Account Name:   `YVONNE PESTANO` 
- Account Number: `3309-0100-34` 
- Type:           `SAVINGS`

<img src="https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/8cefb90925dda27a716fe07e52970460c0aa0a7b/images/qr_codes/bpi.png" alt="BPI QR Code" width="334"/>

****

> METRO BANK

- Metropolitan Bank & Trust Co. 
- Name:   `YVONNE P. PEDRAITA` 
- Number: `0023-00261-2403` 
- Type:   `SAVINGS`

<img src="https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/0209371168b14292330bb1c73b43ca606293b79c/images/qr_codes/metro.png" alt="Metrobank QR Code" width="334"/>



****
> GCASH

- Number: `0928-6517-483`

<img src="https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/8cefb90925dda27a716fe07e52970460c0aa0a7b/images/qr_codes/gcash.png" alt="GCash QR Code" width="334"/>
