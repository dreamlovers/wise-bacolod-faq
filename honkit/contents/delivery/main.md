# Delivery Options 

> **LBC** 

- For customers outside Bacolod City we typically use LBC.
- Use [this](https://www.lbcexpress.com/track/) LBC site to track your packages.


> **MAXIM RIDER** 

- As for those within Bacolod City, this is our preferred delivery service to use.
- You can download the app in this [site](https://taximaxim.com/ph/?referer=self) or use it's web version there. 


> **PICKUP ** 

- Alternatively you can pickup your orders in this location.
- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2244.1437488943598!2d122.9702355913443!3d10.669065188358596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33aed1ccb0e24c6f%3A0x6b2ffa6236e0ed84!2sWISE%20Bacolod!5e0!3m2!1sen!2sjp!4v1640951588705!5m2!1sen!2sjp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe> 
- View in [Google Map](https://goo.gl/maps/J2J7fe3Y8nd12etm6)
