# Summary

* [Home](README.md)
* [Wedding Invitation Parts](./anatomy.md)
* [Packages](./packages/comparison.md)
* [Other Works](./other_works/main.md)
* [Delivery Options](./delivery/main.md)
* [Payment Methods](./payment/main.md)
* [Rush Order](./rush_order/main.md)
* [Giveaway Ideas](./giveaways/main.md)
* [FAQ](./faq/main.md)
* [Terms & Disclaimer](./terms/main.md)

<!--
  * Packages
    * [Package I](./packages/package_i.md)
    * [Package II](./packages/package_ii.md)
    * [Package III](./packages/package_iii.md)
    * [Comparison](./packages/comparison.md)
-->
