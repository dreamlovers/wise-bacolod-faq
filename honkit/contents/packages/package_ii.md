# PACKAGE II 

**PHP 125** *(Starts at)*
- MINIMUM ORDER OF 50 PCS

**INVITATION DETAILS** 

![Bella](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/e91860aaaf28462d4cb412d0c1b2ed07b32f3009/images/packages/package_b_bella.png)

![Del Rey](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/a8b751fbcbac247b9bb1bdc8c23cd4fbc188d95a/images/packages/package_b_delrey.png)


****

**INCLUSIONS:**
  - 5x7 IN. BARONIAL, CUSTOM ENVELOPE COLOR 250 GSM
  - 2-PAGE PRINTS 5x7 IN. 250 GSM
  - 1-PAGE 3.5x5 IN 250 GSM
  - ANY LAYOUT DESIGN AVAILABLE
  - ON-HAND DESIGN WAX SEAL (DEFAULT) OR EMBLEM ON DIECUT BOARD FRAME
  - CHOOSE BETWEEN EMBOSSED ENVELOPE FLAP OR ENVELOPE LINER

****
**ADD-ONS:**
  - ENVELOPE LINER PHP15
  - EMBOSSED ENVELOPE FLAP PHP15
  - ENVELOPE: UV PRINT CALLIGRAPHY / EMBOSSED UV EMBLEM PRINTING PHP45
  - TRANSPARENT WRAP PHP30
  - DRIED FLOWER PHP45
  - JUTE STRING PHP10
  - LETTERPRESS NAMES PHP50 (MIN OF 100 PCS)
  - FULL LETTERPRESS PHP75/PAGE (MIN OF 100 PCS)
  - FLAT FOIL STAMP NAMES PHP50
  - ACRYLIC PAGE 5x7 PHP160 (MIN OF 30 PCS)
  - ADDITIONAL PRINT
    - 5x7 PHP25/PAGE
    - 3.5x5 PHP15/PAGE 
  - GATEFOLD LASERCUT/DIECUT COVER STARTS @ PHP45
  - POCKETFOLD LASERCUT/DIECUT COVER STARTS @ PHP65
  - CUSTOMIZED WAX SEAL STAMP PHP2,000
  - CUSTOMIZED RIBBON STARTS @ PHP600/ROLL PRINTED 50 YARDS
