# Package Comparison 

|                    | Package I   | Package II  | Package III |
| ------------------ | ----------- | ----------- | ----------- |
|Price starts at 📦  | **PHP95**   | **PHP125**  | **PHP150**      |
|                     |[![Lana](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/aabb4c1944522c4e2e59cc521c05093cf940ef2f/images/packages/package_a_lana.png)](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/aabb4c1944522c4e2e59cc521c05093cf940ef2f/images/packages/package_a_lana.png) | [![Bella](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/e91860aaaf28462d4cb412d0c1b2ed07b32f3009/images/packages/package_b_bella.png)](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/e91860aaaf28462d4cb412d0c1b2ed07b32f3009/images/packages/package_b_bella.png) | [![Olivia3](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/db5f7ce66ab608610de42d9d2282b8dc39bf5367/images/packages/package_01_olivia_03.png)](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/db5f7ce66ab608610de42d9d2282b8dc39bf5367/images/packages/package_01_olivia_03.png) |
| Envelope 📦  | <li>BARONIAL style</li> <li>WHITE or CREAM color 140 gsm</li> | <li>BARONIAL style</li> <li>CUSTOM envelope color 250 gsm</li> <li>EMBOSSED SEAL FLAP</li> | <li>BARONIAL style</li>  <li>CUSTOM envelope color 250 gsm</li> <li> EMBOSSED SEAL FLAP</li> |
| MAIN & ENTOURAGE pages 📦  | <li>5x7 inches 250 gsm</li> <li>WHITE MATTE BOARD</li> | <li>5x7 inches 280 gsm</li> <li>OFF WHITE or CREAM BOARD</li> | <li>6x8 inches 280 gsm</li> <li> OFF WHITE or CREAM BOARD</li>|
| RSVP CARD 📦               | 3.5x5 inches 250 gsm | 3.5x5 inches 280 gsm | 4x6 inches 280 gsm |
| Embellishment 📦           | DIECUT BOARD EMBLEM  | On-hand DESIGN WAX SEAL (default) or DIECUT BOARD EMBLEM |On-hand WAX SEAL (default) or DIECUT BOARD EMBLEM |
| Per package add-ons 🛒     |<li>Choice of COLORED ENVELOPE (250 gsm) PHP25</li><li>EMBOSSED SEAL FLAP PHP10</li>  <li>EMBOSSED SIDE FLAPS PHP15</li> <li>ACRYLIC PAGE 5x7 PHP160 (min of 30 pcs)</li> <li>Additional 5x7 PRINT PHP25 /page</li> <li>Additional 3.5x5 PHP15 /page</li> <li>GATEFOLD LASERCUT or DIECUT COVER starts @ PHP45</li> <li>POCKETFOLD LASERCUT or DIECUT COVER starts @ PHP65</li> <li>On-hand DESIGN WAX SEAL PHP15</li>|<li>EMBOSSED SIDE FLAPS PHP15</li> <li>ACRYLIC PAGE 5x7 PHP160 (min of 30 pcs)</li> <li>Additional 5x7 PRINT PHP25 /page</li> <li>Additional 3.5x5 PHP15 /page</li> <li>GATEFOLD LASERCUT or DIECUT COVER starts @ PHP45</li> <li>POCKETFOLD LASERCUT or DIECUT COVER starts @ PHP65</li> |<li>EMBOSSED SIDE FLAPS PHP15</li> <li>ACRYLIC PAGE 6x8 PHP180 (min of 30 pcs)</li> <li>Additional 6x8 PRINT PHP30 /page</li> <li>Additional 4x6 PHP20 /page</li> <li>GATEFOLD LASERCUT or DIECUT COVER starts @ PHP75</li> <li>POCKETFOLD LASERCUT or DIECUT COVER starts @ PHP95</li>  |
| <img width=1000/>| <img width=20000/> | <img width=20000/> | <img width=20000/> |


## Package Independent 
| Item                               | Details               | Notes |
| -------------                      | ----------------      |    |    
| Minimum number of invite order 📦  | 50 pcs                |    | 
| Layout design 📦                   | Any available         |    |
| ENVELOPE LINER 🛒                  | PHP15 /pc             |    |
| UV CALLIGRAPHY      🛒             | PHP45 /pc             | UV print of names on envelope |
| EMBOSSED UV EMBLEM  🛒             | PHP45 /pc             | UV print on envelope with emboss effect |
| TRANSPARENT WRAP  🛒               | PHP30 /pc             |    |
| DRIED FLOWER 🛒                    | PHP45 /pc             |    |
| JUTE STRING  🛒                    | PHP10 /pc             |    |
| LETTERPRESS NAMES 🛒               | PHP50 /name           | Minimum of 100 pcs invite order |
| FULL LETTERPRESS  🛒               | PHP75 /page           | Minimum of 100 pcs invite order |
| FLAT FOIL STAMP NAMES 🛒           | PHP50 /name           |    |
| CUSTOMIZED WAX SEAL STAMP 🛒       | PHP2,000              |    |
| CUSTOMIZED RIBBON 🛒               | Starts @ PHP600 /roll | 50-yard printed ribbon |
| <img width=15/>| <img width=20/> | <img width=300/>  |


## Legend
- Items marked with 📦 are included in the package. 
- Items marked with 🛒 are **non-free** add-ons, to be purchased separately from the package.

<style>
#book-search-results
{
  width: 95em;
}
.page-inner
{
  margin-left: 2em;
}
</style>

