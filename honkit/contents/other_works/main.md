# Other Works 

> **UV Print on Acrylic** 

- The Vault  

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FWISEbacolod%2Fposts%2Fpfbid02Kc4Nc795jKLuqCa5gycan5FhTBvykpyGhJ1gQ9cZVYDCmbxvs1h5MViC8c23QmGVl&show_text=false&width=500" width="500" height="281" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>

<br>

> **Classic Material** 

|                    |             |
| ------------------ | ----------- |
| <li> Endless Ocean </li><iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FWISEbacolod%2Fposts%2Fpfbid0B9Nw8k1qzz7x4m6BdVNNeJBFJ6RBj4AUq29ib4edYaT2tCr6oqwk1mM73skEEXR6l&show_text=false&width=500" width="500" height="314" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>| <li> New Zealand Flower </li> <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FWISEbacolod%2Fposts%2Fpfbid05KbJ7tcvYwnKKDdCApg1dEgAMdHgdwA7fBdmuEyTC2agTPBiVARLQ9LVnx2P1mSal&show_text=false&width=500" width="500" height="281" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe> |
| <li> Nautical </li> <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FWISEbacolod%2Fposts%2Fpfbid0x1kFz7bS7ozJM23GoVDuCE9s7J5zuTF7QNfVShMzxS7AjPcQkuRiyBJhqRVB3MWYl&show_text=false&width=500" width="500" height="375" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>| <li> Filipiñana </li> <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FWISEbacolod%2Fposts%2Fpfbid0kZe59h3vhdv8e29npxpZNEAMTnZR6VMMbQKN8GbpgoMPx4xmLu4HJYJ3CFE5Zop1l&show_text=false&width=500" width="500" height="375" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>|
| <li> Anchorage </li> <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FWISEbacolod%2Fphotos%2Fa.441532362693124%2F1952759661570379%2F%3Ftype%3D3&show_text=false&width=500" width="500" height="703" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>| <li> Nemophila </li> <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FWISEbacolod%2Fphotos%2Fa.441532362693124%2F1256163724563313%2F%3Ftype%3D3&show_text=false&width=500" width="500" height="645" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe> |


<style>
#book-search-results
{
  width: 76em;
}
.page-inner
{
  margin-left: 2em;
}
</style>
