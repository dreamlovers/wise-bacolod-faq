# Giveaway Ideas 

> FOR SPONSORS 


#### TUMBLERS

  ![Tumblers](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/9c86d75026495d0323bb6a5dd6e0aa3aad4f3475/images/giveaways/tumbler.png)

- PHP 550 
- Custom print & personalized  
- [More samples](https://www.facebook.com/media/set/?set=a.1945864765593202&type=3)


****
> FOR GENERAL ATTENDIES 


#### ALCOHOL MIST SPRAY

  ![Mist Spray](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/9c86d75026495d0323bb6a5dd6e0aa3aad4f3475/images/giveaways/mist_spray.png)

- PHP 180 
- Custom print & personalized  
- [More samples](https://www.facebook.com/media/set/?set=a.1869143233265356&type=3)



#### FRIDGE MAGNET

  ![Fridge Magnet](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/9c86d75026495d0323bb6a5dd6e0aa3aad4f3475/images/giveaways/fridge_magnet.png)

- PHP 75 
- MINIMUM: 35 PCS 
- Custom print & personalized  
- [More samples](https://www.facebook.com/media/set/?set=a.1772043669641980&type=3)


#### CARFUME 

  ![Carfume](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/cc351047edf8efcd78a66627beafcdd3135709f1/images/giveaways/carfume.png)

- PHP 125 
- MINIMUM: 35 PCS 
- Custom print & personalized  
- [More samples](https://www.facebook.com/media/set/?vanity=WISEbacolod&set=a.2080131752166502)


****
`NOTE`

- Personalized print in relation to the bride and the groom. e.g. Wedding/debutant emblem

