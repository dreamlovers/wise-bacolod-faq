# Terms & Disclaimer 

> FORM FILL UP 

- You are only allowed **three (3) times** to submit your modifications
to the layout & design,including the names on your invitation. Starting on
the fourth modification we will be requiring **300 PHP** on simple modification. A change of design
would be **1500 PHP**.

- It is a **MUST** that you provide a complete list of the entourage.
The speed of our processing will depend on the completeness and accuracy
of the details you provide.

- Provide the correct spelling and specify titles or honorifics
(e.g. Mr. Ms. Engr. Dr. , etc.). We will copy and paste these names to
minimize typographical errors ( `typo` ). Not using this format will create 
delays for our artist.

- You may add middle initials for the names. 
There is an added formality if the names have middle initials. Beware though
that it takes time to ask each person of their middle initial and in turn may
affect your schedule. 

- If you choose **to have middle initials** on the names, for the purpose of
uniformity every single name **SHOULD** have a middle initial.

- Invitations to be processed less than **15 days** of the regular processing time 
are considered **RUSH INVITES**. Expect the possibility of delays. With that,
prompt response and thorough provision of details from you is a **MUST**. 
With your patience and cooperation we will do our best to speed up the process.

- Leave the not applicable details `BLANK`. If some details are not provided, we will
be using our template. 

- `PRINCIPAL SPONSORS` usually come in pairs. Kindly note we print the names of the
`PRINCIPAL SPONSORS` according to the order/sequence you provided. If you want,
you can arrange them alphabetically by yourself.

> LAYOUT & DESIGN

- We highly suggest that you use our designated form / file for the details
of the event. Layout process may take **2 to 3 days**. It may be longer if the
design is new or complicated. 

- A fee may apply if a vector/design has to be purchased or drawn manually.
Please settle the required payment for us to start your order.

> RELEASE

- Your invitiations will be released **15 days** after you approved the printing
and production. It will take longer time if requires multi-pass print or the details
are intricate.

> CANCELLATION

- **STRICTLY NO CANCELLATIONS.**
- We will not refund any payment made. However if the order has 
not started, you may choose to get a replacement product or service instead.

> DISCLAIMER

- Please note that it is your responsibility to **CHECK WORD FOR WORD** for your inputs.

- Our artist edit/handle multiple invites on a daily basis. Our artist may misspell
some words or make grammatical errors. You can ask someone else to review it for you.

- We will not be responsible for any typo or missing details if you already approved
printing and have given the signal to proceed to production.

- To ascertain your expectations about the invitation you may request for a paid mockup
(physical sample), price starts at PH 500. 

- Whether you choose to have a mockup or not, **WE DO NOT REPLACE** the invites once
released. 


 ```
 PLEASE PROOFREEAD BEFORE YOU APPROVE PRINTING AND PRODUCTION.
 ```

- Packaged invite are modified to fit package allowance, you may opt to upgrade at
your own expense.

- As necessary, we will automatically add extra pages in the invite (extra page means additional charges).
Too much text/entourage list on a page will downgrade our design or it will make it less appealing.

- We will not be responsible for the loss or damage of the delivery of the invites.
It will be charged against the delivery service and not with us. 

- **THE ACT OF PROCEEDING WITH THE ORDER MEANS YOU AGREE WITH OUR TERMS AND DISCLAIMER.**

> LAST FEW THINGS..

- We reserve the right to modify the contents of these web pages without prior notice to our customers. 

- We reserve the right to `PROTECT` our `BRAND INTEGRITY. `

- Enjoy the rest of your event or preparations! 🍀 

