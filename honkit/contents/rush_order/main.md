# Rush Order 

> GENERAL CONDITIONS 

Before moving forward with your order we need to **satisfy and agree with these conditions**.

 - Minimum quantity should be `40 PCS`. 
 - Due to `PEAK SEASON` or production situation we may decline your order.
 - Materials should be available, choice of paper, envelopes, wax seal and relevant color motif. 
 - Only simple invites, e.g. print and cut only. `FOIL PRESSED` and `ACRYLIC INVITES` can't be rushed. 
 - Only existing design will be used, we can't rush the customized ones. 
 - `RUSH FEE`, the price will be `30% more` as compared to non-rush orders. 
 - At the start of the transaction we require `70% DOWN PAYMENT`. 
 - Wedding details should be finalized and submitted. These include names, venue, date & time, etc.


> PROCESS & TIMELINES 

Due to the time-dependent nature of the transcation, we expect you to **also agree with the following:**

![Rush Order](https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/10edaf6581d2b901a4555529318dfe885cbf7177/images/rush_order/process.jpeg)
