# Frequently Asked Questions (FAQ) 

> PROCESS 

- What are the stages in invitation card creation?
  1. *Inquiry                 *  
  1. *Package selection       * 
  1. *Order form completion   * 
  1. *Quotation & Down payment* 
  1. *Layout & Design         * 
  1. *Approval & Production   * 
  1. *Full Payment & Release  *  


- What is the production lead time?
  - `3 to 4 weeks` before release date.
  - `2 weeks and below` is considered a `rush order`
  - See the [Rush Order](https://wise-bacolod.web.app/rush_order/main.html) section.
  
> INQUIRY

- What is the best time of the day to receive response with my inquiries?
  - `11 am - 12 nn`
  - `5 pm - 6 pm`


- Where should I do my inquiries? 
  - You can inquire [here](https://www.facebook.com/messages/t/441475156032178) or [here](https://www.facebook.com/messages/t/1800987117)


- Do you have a mobile phone number to call to?
  - Yes please contact us at `+63 985 916 6036`
  - Ask for `WISE Bacolod`

> PACKAGE / DESIGN 


- Do you have existing packages?
  - Yes please check the [Packages Comparison](https://wise-bacolod.web.app/packages/comparison.html) section. 


- Can I suggest my own design / theme?
  - Yes


- Can I have the flat image or e-invitation?
  - Yes, for PHP 500 you can avail them.


- Do I need to pay the layout / design fee?
  - No if you satisfy the `minimum order quantity` 
  - Otherwise, the layout fee starts at PHP 850, depending on the layout


- Is there a mockup so that I can see how the invitation card look like?
  - Yes but we plan to remove it in the future.


> ORDER

- What is the `minimum order quantity` ?
  - **Quantities:** 
    - `50 pcs` for wedding & debut
    - `30 pcs` for baptism & birthday 

  - **For additional copies after release:**
    - `10 pcs` for wedding/ debut/ baptism/ birthday
    - `10% Add-on fee` will be charged
     

- How to order?
  - Inquire for the order form [here](https://www.facebook.com/messages/t/441475156032178) or [here](https://www.facebook.com/messages/t/1800987117)


> PAYMENT

- Can I skip paying the down payment?
  - `NO` a down payment is required to proceed.

- How much is the down payment?
  - We require a `Layout Fee` starting at PHP 850, depending upon the complexity of the design. This amount will be deducted to the total amount due once you proceed booking your invitation with us.
  - We require a `70%`down payment before `Production` stage.


- How much is the extra fee for the rush oder?
  - `30%` of the original total price will be the amount for rush order fee.
  - See the [Rush Order](https://wise-bacolod.web.app/rush_order/main.html) section.


- How do I pay?
  - See the [Payment Methods](https://wise-bacolod.web.app/payment/main.html) section.


> DELIVERY

- How can I get my invitations?
  - See the [Delivery Options](https://wise-bacolod.web.app/delivery/main.html) section.


- Do you offer free delivery?
  - `NO` we don't offer free delivery.
