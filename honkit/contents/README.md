<head>
  <meta charset="UTF-8">
  <meta name="content" content="Wedding Invitations Supplies and etc.">
  <meta name="about" content="Maker and supplier of wedding invitation.">
  <meta name="location" content="Bacolod City">
</head>

#  <img src="https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/4fe867d4a7f1fd7a716445228f137d0a28b346b5/images/logo.jpeg" alt="WISE Bacolod" width="60"/>

- **HOW TO USE**
  - We highly suggest that you visit the [Wedding Invitation Parts](./anatomy.md) first to get yourself acquainted with our product. 
  - To show the side-bar click or tap the hamburger icon: <img src="https://bitbucket.org/dreamlovers/wise-bacolod-faq/raw/cab149e2e396981c88f81113a1ee6ca675e3ba6f/images/hamburger.png" width="30" > on the top left.
  - Use the search box, see `Type to search`. As you write something, matching pages will show up.
  - Just delete what you wrote to clear the matched pages.
  - You can directly click or tap any item of interest in the side-bar. 

- 👍 or contact us on our [Facebook page](https://www.facebook.com/WISEbacolod)

<br>
<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fphoto%2F%3Ffbid%3D620976869453622%26set%3Da.603462121205097&show_text=false&width=500" width="500" height="479" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>

<br>
© 2022 by Vanguard Digital Bacolod 
