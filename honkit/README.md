# Installation

```
npm install
```

# Running Honkit Server
- Live reload is automatic when target files are being edited

```
npx honkit serve 
```

# Building 

```
npx honkit build
```

# Deploying to Firebase
- Make sure to login and select `wiserp-docs`

```
firebase login 

firebase deploy
```
